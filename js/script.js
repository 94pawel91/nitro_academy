function validateEmail(email) {

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validate() {

    var email = document.getElementById("email").value;
    var remail = document.getElementById("remail").value;
    var name = document.getElementById("name").value;
    var rname = document.getElementById("rname").value;
    var message = document.getElementById("message").value;
    var vouchers = document.getElementById("vouchers").value;
    var radioNow = document.getElementById("radio-now");
    var radioChoose = document.getElementById("radio-choose");
    var day = document.getElementById("day").value;
    var month = document.getElementById("month").value;
    var year = document.getElementById("year").value;

    if (validateEmail(email) && validateEmail(remail)) {

        var buttonRadio = "Not selected";

        if (radioNow.checked) {
            buttonRadio = "Now";
        }

        else if (radioChoose.checked) {
            buttonRadio = "Choose Date";
        }

        if (vouchers == "£100") {
            alert("This voucher is not available");
        }

        else {
            alert("From: " + email + "\n" +
                "Your email address: " + remail + "\n" +
                "To: " + name + "\n" +
                "Recipient's email address: " + rname + "\n" +
                "Vouchers Amount: " + vouchers + "\n" +
                "Button Radio: " + buttonRadio + "\n" +
                "Date: " + "Day: " + day + " / Month: " + month + " / Year: " + year + "\n" +
                "Message: " + message);
        }

        return true;
    }
    else {

        alert("Incorrect data");
        return false;

    }
}

document.getElementById("main-form").addEventListener("submit", function (e) {

    e.preventDefault();
    validate();

}, false);
